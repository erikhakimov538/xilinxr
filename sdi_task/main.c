// #################################################################################################
// # << NEORV32 - Serial Data Interface Demo Program >>                                            #
// # ********************************************************************************************* #
// # BSD 3-Clause License                                                                          #
// #                                                                                               #
// # Copyright (c) 2023, Stephan Nolting. All rights reserved.                                     #
// #                                                                                               #
// # Redistribution and use in source and binary forms, with or without modification, are          #
// # permitted provided that the following conditions are met:                                     #
// #                                                                                               #
// # 1. Redistributions of source code must retain the above copyright notice, this list of        #
// #    conditions and the following disclaimer.                                                   #
// #                                                                                               #
// # 2. Redistributions in binary form must reproduce the above copyright notice, this list of     #
// #    conditions and the following disclaimer in the documentation and/or other materials        #
// #    provided with the distribution.                                                            #
// #                                                                                               #
// # 3. Neither the name of the copyright holder nor the names of its contributors may be used to  #
// #    endorse or promote products derived from this software without specific prior written      #
// #    permission.                                                                                #
// #                                                                                               #
// # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS   #
// # OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF               #
// # MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE    #
// # COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     #
// # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE #
// # GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED    #
// # AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING     #
// # NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED  #
// # OF THE POSSIBILITY OF SUCH DAMAGE.                                                            #
// # ********************************************************************************************* #
// # The NEORV32 Processor - https://github.com/stnolting/neorv32              (c) Stephan Nolting #
// #################################################################################################


/**********************************************************************//**
 * @file demo_sdi/main.c
 * @author Stephan Nolting
 * @brief SDI test program (direct access to the SDI module).
 **************************************************************************/

#include <neorv32.h>
#include <string.h>
#include <sdi.h>



/**********************************************************************//**
 * @name User configuration
 **************************************************************************/
/**@{*/
/** UART BAUD rate */
#define BAUD_RATE 19200
/**@}*/

//структура управления блоками питания
struct type_POL {
	unsigned int pol_snapshot_time;
	enum  	{	 INIT,
			 	 IDLE,
			 	 STEP1,
			 	 STEP2,
			 	 STEP3,
			 	 STEP4,
			 	 STEP5,
			 	 STEP6,
			 	 MONITOR,
			 	 DELAY_PG,
			 	 ALARM,
			 	 DELAY_RESET
			 } pol_state, pol_next;
	unsigned char pol_message;
};

//структура управления вентиляторами
struct type_FAN {
	unsigned int fan_snapshot_time;
	enum  	{	 INIT,
			 	 IDLE,
			 	 STEP1,
			 	 STEP2,
			 	 STEP3,
			 	 STEP4,
			 	 STEP5,
			 	 STEP6,
			 	 MONITOR,
			 	 DELAY_PG,
			 	 ALARM,
			 	 DELAY_RESET
			 } fan_state, fan_next;
	unsigned char fan_message;
};


/**********************************************************************//**
 * This program provides an interactive console for the SDI module.
 *
 * @note This program requires UART0 and the SDI to be synthesized.
 *
 * @return Irrelevant.
 **************************************************************************/
int main() {

  char buffer[8];
  int length = 0;

  // capture all exceptions and give debug info via UART
  neorv32_rte_setup();

  // setup UART at default baud rate, no interrupts
  neorv32_uart0_setup(BAUD_RATE, 0);

  // check if UART0 unit is implemented at all
  if (neorv32_uart0_available() == 0) {
    return 1;
  }

  // intro
  neorv32_uart0_printf("\n<<< SDI Test Program >>>\n\n");

//====================================== mtime section ============================================
  if (neorv32_mtime_available() == 0) {
    neorv32_uart0_puts("ERROR! MTIME timer not implemented!\n");
    return 1;
  }



//======================================== SDI section ============================================
  // check if SDI unit is implemented at all
  if (neorv32_sdi_available() == 0) {
    neorv32_uart0_printf("ERROR! No SDI unit implemented.");
    return 1;
  }

  // info
  neorv32_uart0_printf("This program allows direct access to the SDI module.\n"
                       "Type 'help' to see the help menu.\n\n");

  // setup SDI module
  neorv32_sdi_setup(0); // no interrupts

  while (1) {
    uint8_t data = sdi_get();
    if (data == 0x40) {
        sdi_put();
    } else {
      neorv32_uart0_printf("Incorrect comand from master...\n");
    }
  }
  return 0;
}
