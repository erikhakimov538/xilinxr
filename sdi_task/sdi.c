#include <sdi.h>

/**********************************************************************//**
 * Write data to SDI TX buffer.
 **************************************************************************/
void sdi_put(void) {

  char terminal_buffer[3];

  neorv32_uart0_printf("Enter TX data (2 hex chars): 0x");
  neorv32_uart0_scan(terminal_buffer, sizeof(terminal_buffer), 1);
  uint32_t tx_data = (uint32_t)hexstr_to_uint(terminal_buffer, strlen(terminal_buffer));

  neorv32_uart0_printf("\nWriting 0x%x to SDI TX buffer... ", tx_data);

  if (neorv32_sdi_put((uint8_t)tx_data)) {
    neorv32_uart0_printf("FAILED! TX buffer is full.\n");
  }
  else {
    neorv32_uart0_printf("ok\n");
  }
}


/**********************************************************************//**
 * Read data from SDI RX buffer.
 **************************************************************************/
uint8_t sdi_get() {

  uint8_t rx_data;

  if (neorv32_sdi_get(&rx_data)) {
    neorv32_uart0_printf("No RX data available (RX buffer is empty).\n");
    return 0;
  }
  else {
    neorv32_uart0_printf("Read data: 0x%x\n", (uint32_t)rx_data);
    return rx_data;
  }
}


/**********************************************************************//**
 * Helper function to convert N hex chars string into uint32_T
 *
 * @param[in,out] buffer Pointer to array of chars to convert into number.
 * @param[in,out] length Length of the conversion string.
 * @return Converted number.
 **************************************************************************/
uint32_t hexstr_to_uint(char *buffer, uint8_t length) {

  uint32_t res = 0, d = 0;
  char c = 0;

  while (length--) {
    c = *buffer++;

    if ((c >= '0') && (c <= '9'))
      d = (uint32_t)(c - '0');
    else if ((c >= 'a') && (c <= 'f'))
      d = (uint32_t)((c - 'a') + 10);
    else if ((c >= 'A') && (c <= 'F'))
      d = (uint32_t)((c - 'A') + 10);
    else
      d = 0;

    res = res + (d << (length*4));
  }

  return res;
}
