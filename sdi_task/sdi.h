#include <neorv32.h>
#include <string.h>

void sdi_put(void);
uint8_t sdi_get();
uint32_t hexstr_to_uint(char *buffer, uint8_t length);