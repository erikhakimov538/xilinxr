// #################################################################################################
// # << NEORV32 - "Hello World" Demo Program >>                                                    #
// # ********************************************************************************************* #
// # BSD 3-Clause License                                                                          #
// #                                                                                               #
// # Copyright (c) 2023, Stephan Nolting. All rights reserved.                                     #
// #                                                                                               #
// # Redistribution and use in source and binary forms, with or without modification, are          #
// # permitted provided that the following conditions are met:                                     #
// #                                                                                               #
// # 1. Redistributions of source code must retain the above copyright notice, this list of        #
// #    conditions and the following disclaimer.                                                   #
// #                                                                                               #
// # 2. Redistributions in binary form must reproduce the above copyright notice, this list of     #
// #    conditions and the following disclaimer in the documentation and/or other materials        #
// #    provided with the distribution.                                                            #
// #                                                                                               #
// # 3. Neither the name of the copyright holder nor the names of its contributors may be used to  #
// #    endorse or promote products derived from this software without specific prior written      #
// #    permission.                                                                                #
// #                                                                                               #
// # THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS   #
// # OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF               #
// # MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE    #
// # COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,     #
// # EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE #
// # GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED    #
// # AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING     #
// # NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED  #
// # OF THE POSSIBILITY OF SUCH DAMAGE.                                                            #
// # ********************************************************************************************* #
// # The NEORV32 Processor - https://github.com/stnolting/neorv32              (c) Stephan Nolting #
// #################################################################################################


/**********************************************************************//**
 * @file hello_world/main.c
 * @author Stephan Nolting
 * @brief Classic 'hello world' demo program.
 **************************************************************************/

#include <neorv32.h>


/**********************************************************************//**
 * @name User configuration
 **************************************************************************/
/**@{*/
/** UART BAUD rate */
#define BAUD_RATE 19200
/**@}*/

typedef struct
{
  uint32_t firmware_version;
  uint32_t crc32;
  uint32_t firmware_len2;
  uint32_t base_addr;
} sFirmwareHeader;

const sFirmwareHeader __attribute__ ((section(".inform_and_crc"))) FirmwareHeader = {
    0x01,         //firmware version
    0x34967ae6,   //crc
    0x198b,       //len in hex
    0x00,         //base addr in flash
};


// Prototypes
void sdi_put(void);
void sdi_get(void);
uint32_t hexstr_to_uint(char *buffer, uint8_t length);

/**********************************************************************//**
 * Main function; prints some fancy stuff via UART.
 *
 * @note This program requires the UART interface to be synthesized.
 *
 * @return 0 if execution was successful
 **************************************************************************/
int main() {

  // capture all exceptions and give debug info via UART
  // this is not required, but keeps us safe
  neorv32_rte_setup();

  // setup UART at default baud rate, no interrupts
  neorv32_uart0_setup(BAUD_RATE, 0);

  // check implemented modules
  if (neorv32_sdi_available() == 0) {
    neorv32_uart0_printf("ERROR! No SDI unit implemented.");
    return 1;
  }

  // if (neorv32_pwm_available() == 0) {
  //   neorv32_uart0_printf("ERROR: PWM module not implemented!\n");
  //   return 1;
  // }

  // if (!neorv32_onewire_available()) {
  //   neorv32_uart0_printf("Error! ONEWIRE module not synthesized!\n");
  //   return -1;
  // }

  // if (neorv32_twi_available() == 0) {
  //   neorv32_uart0_printf("No TWI unit implemented.");
  //   return 1;
  // }


  // check number of PWM channels
  int num_pwm_channels = neorv32_pmw_get_num_channels();
  neorv32_uart0_printf("Implemented PWM channels: %i\n\n", num_pwm_channels);

  // deactivate all PWM channels
  for (int i=0; i<num_pwm_channels; i++) {
    neorv32_pwm_set(i, 0);
  }

  // configure and enable PWM
  neorv32_pwm_setup(CLK_PRSC_64);




  // check available hardware extensions and compare with compiler flags
  neorv32_rte_check_isa(0); // silent = 0 -> show message if isa mismatch

  // print project logo via UART
  neorv32_rte_print_logo();

  // say hello
  neorv32_uart0_puts("Hello world! :)\n");


  return 0;
}
