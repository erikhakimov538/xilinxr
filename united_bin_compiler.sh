#!/bin/bash
rm neorv32_raw_exe.bin
cd demo_xip
# ������ �������� ������������� � ������� ���������
make MARCH=rv32i USER_FLAGS+="-Wl,--defsym,__neorv32_rom_base=0xe0000000" clean_all bin
cd ..
cd hello_world
# ��������� �������� ������������� � �������� � 1�� (0x00100000 = 1��)
make MARCH=rv32i USER_FLAGS+="-Wl,--defsym,__neorv32_rom_base=0xe0100000" clean_all bin
cd ..
# ������ �������� ���� ������������ � ������, � ������ �� �������� � 1�� (2048=1k=1��)
dd if=demo_xip/neorv32_raw_exe.bin of=neorv32_raw_exe.bin oflag=append conv=notrunc
dd if=hello_world/neorv32_raw_exe.bin of=neorv32_raw_exe.bin oflag=append seek=2048