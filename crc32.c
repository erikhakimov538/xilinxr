#include <stdio.h>
#include <stdint.h>
#include <malloc.h>

unsigned long Crc32( unsigned char *buf,  unsigned long len) {
    unsigned long  crc_table[256];
    unsigned long  crc;
    int addr = 0;
    int size = (len - 0x10);

    // ������ ������� crc
    for (int i = 0; i < 256; i++) {
        crc = i;
        for (int j = 0; j < 8; j++)
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320UL : crc >> 1;
        crc_table[i] = crc;
    };

    // �������� �� ������ ������������ ������ 
    buf = buf + 0x10;
    crc = 0xFFFFFFFFUL;
    
    while (len--) {
        // �������� �� ����� �� ������� �����
        if (addr > size) {
            return crc ^ 0xFFFFFFFFUL; //����� �������
        }

        // if (addr <= 0x200){
            // printf("0x%x     ", addr+0x10);
            // printf("0x000000%x     ", *buf);
        // }

        // ���������� �������� �������� crc
        crc = crc_table[(crc ^ *buf++) & 0xFF] ^ (crc >> 8);

        // if (addr <= 0x200){
            // printf("0x%x    \n", crc);
        // }
        addr++;
    }
    return crc ^ 0xFFFFFFFFUL; //����� �������
};
 
int main() {
    int len = 0;
    FILE *f = fopen("demo_xip/neorv32_raw_exe.bin", "rb");

    // ������ ������� ��������� �����
    while(getc(f) != EOF) len++;
    len--;
    printf("demo_xip/neorv32_raw_exe.bin\n");
    printf("len:    0x%x\n", len);

    // ��������� ������ ��� ������ ��� ������ � ������ � ��� �������� �� ������
    char *buf = (char*) malloc(len);
    unsigned long  Crc32( unsigned char *buf,  unsigned long len);
    if ((f = fopen("demo_xip/neorv32_raw_exe.bin", "rb"))==NULL) {
        printf ("File open ERROR...");
        return 0;
    }
    
    // ����� ������� ��� ������� ����������� �����
    fread(buf, sizeof(char), len, f);
    printf("CRC:    %p\n", Crc32(buf,len));
    fclose(f);

    // CRC ��� ���������� ��������� �����
    FILE *t = fopen("hello_world/neorv32_raw_exe.bin", "rb");
    len = 0;

    // ������ ������� ��������� �����
    while(getc(t) != EOF) len++;
    len--;
    printf("hello_world/neorv32_raw_exe.bin\n");
    printf("len:    0x%x\n", len);

    // ��������� ������ ��� ������ ��� ������ � ������ � ��� �������� �� ������
    char *buf2 = (char*) malloc(len);
    unsigned long  Crc32( unsigned char *buf,  unsigned long len);
    if ((t = fopen("hello_world/neorv32_raw_exe.bin", "rb"))==NULL) {
        printf ("File open ERROR...");
        return 0;
    }
    
    // ����� ������� ��� ������� ����������� �����
    fread(buf2, sizeof(char), len, t);
    printf("CRC:    %p\n", Crc32(buf2,len));
    fclose(t);
    return 0;
}
